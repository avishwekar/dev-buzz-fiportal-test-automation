package com.buzzpoints.selenium.test.app.merchantportal.suites;

import com.buzzpoints.selenium.api.TestConfig;
import com.buzzpoints.selenium.api.TestSuite;
import com.buzzpoints.selenium.test.app.merchantportal.cases.CaseCreatePointCampaign;
import com.buzzpoints.selenium.test.app.merchantportal.cases.CaseCreateOffer;
import com.buzzpoints.selenium.test.app.merchantportal.cases.CaseCreateReward;
import com.buzzpoints.selenium.test.app.merchantportal.cases.CaseEndReward;
import com.buzzpoints.selenium.test.app.merchantportal.cases.CaseLogin;

/**
 * Created by mlittman on 11/18/15.
 */
@Deprecated
public class SuiteCreateCampaigns extends TestSuite {

	public SuiteCreateCampaigns(TestConfig cfg) {
		super(cfg);
		setup();
	}

	protected void setup() {
		//Login test case
		this.getTestCaseList().add(new CaseLogin(getConfig()).describe("Merchant Portal Login", ""));

		//Create reward test case
		this.getTestCaseList().add(new CaseCreateReward(getConfig()).describe("Merchant Portal Create Reward", ""));

		//End reward test case
		this.getTestCaseList().add(new CaseEndReward(getConfig()).describe("Merchant Portal End Reward", ""));

		//Create offer test case
		this.getTestCaseList().add(new CaseCreateOffer(getConfig()).describe("Merchant Portal Create Offer", ""));

		//Create point campaign test case
//		this.getTestCaseList().add(new CaseCreatePointCampaign(getConfig()).describe("Merchant Portal Create Point Campaign", ""));
	}
}
