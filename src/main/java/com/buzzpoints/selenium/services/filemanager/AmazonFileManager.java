package com.buzzpoints.selenium.services.filemanager;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.buzzpoints.selenium.services.Settings;

import java.io.File;

/**
 * Created by mlittman on 12/7/15.
 */
public class AmazonFileManager extends AbstractFileManager {
    AWSCredentials credentials = null;
    AmazonS3 s3Client;
    String bucketName;


    public AmazonFileManager(){
        bucketName = (String)Settings.resolve("S3BucketName");

        if(Boolean.valueOf((String)Settings.resolve("AWS_ConfigHere"))){
            String accessKey = (String)Settings.resolve("AWS_AccessKey");
            String secretKey = (String)Settings.resolve("AWS_SecretKey");
            credentials = new AWSCustomCredentials(accessKey, secretKey);
        }
        else{
            try{
                credentials = new ProfileCredentialsProvider().getCredentials();
            }
            catch(Exception e){
                throw new AmazonClientException(
                        "Cannot load the credentials from the credential profiles file. " +
                                "Please make sure that your credentials file is at the correct " +
                                "location (~/.aws/credentials), and is in valid format.",
                        e);
            }
        }


        s3Client = new AmazonS3Client(credentials);
        Region awsRegion = Region.getRegion(Regions.US_EAST_1);
        s3Client.setRegion(awsRegion);
    }

    /**
     * Upload files
     */
    public void uploadFiles() {
        for(File file : getFiles()) {
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, file.getName(), file);
            PutObjectResult putObjectResult = s3Client.putObject(putObjectRequest);
        }
    }

    //Custom AWS Credentials implementation to allow for locally injected credentials
    class AWSCustomCredentials implements AWSCredentials{
        String awsAccessKey;
        String awsSecretKey;

        public AWSCustomCredentials(String access, String secret) {
            awsAccessKey = access;
            awsSecretKey = secret;
        }
        public String getAWSAccessKeyId() {
            return awsAccessKey;
        }

        public String getAWSSecretKey() {
            return awsSecretKey;
        }

    }
}
