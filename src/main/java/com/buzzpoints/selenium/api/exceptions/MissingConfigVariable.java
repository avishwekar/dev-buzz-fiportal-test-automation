package com.buzzpoints.selenium.api.exceptions;

import java.util.Iterator;
import java.util.List;

/**
 * Created by mlittman on 11/20/15.
 */
@Deprecated
public class MissingConfigVariable extends Exception {
    public MissingConfigVariable(List<String> missingVars){
        super(_generateMessage(missingVars));
    }

    static String _generateMessage(List<String> missingVars){
        StringBuilder output = new StringBuilder();
        output.append("The following variables are missing [");
        Iterator<String> varIter = missingVars.listIterator();
        while(varIter.hasNext()) {
            output.append(varIter.next());
            if(varIter.hasNext())
                output.append(", ");
        }
        output.append("]");
        return output.toString();
    }
}