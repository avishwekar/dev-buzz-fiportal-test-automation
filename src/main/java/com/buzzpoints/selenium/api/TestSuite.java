package com.buzzpoints.selenium.api;

import com.buzzpoints.selenium.api.TestCase.Results;
import com.buzzpoints.selenium.api.exceptions.MissingConfigVariable;
import com.buzzpoints.selenium.main.BuzzTestAutomation;
import com.relevantcodes.extentreports.ExtentReports;
import lombok.Getter;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by mlittman on 11/10/15.
 */
@Deprecated
public abstract class TestSuite {

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");

    private static ExtentReports reporter;
    private static File reportFile;
    @Getter final private TestConfig config;
    @Getter private List<TestCase> testCaseList = new ArrayList<TestCase>();
    @Getter private HashMap<WebDriver, List<TestCase.Results>> resultList = new HashMap<WebDriver, List<Results>>();
    private List<TestRunner> activeRunners = new ArrayList<TestRunner>();

    public TestSuite (TestConfig cfg) {
        config = cfg;
    }

    public TestSuite (TestConfig cfg, List<TestCase> testCases) {
        this(cfg);
        testCaseList.addAll(testCases);
    }

    /**
     * Get static ExtentReports instance
     * @return ExtentReports instance
     */
    public synchronized static ExtentReports getReporter(){
        if(reporter == null){
            createOutputFile();
            reporter = new ExtentReports(reportFile.getPath(), true);
        }
        return reporter;
    }

    /**
     * Attempt to create the output file for the report
     */
    private static void createOutputFile(){
        Timestamp timestamp = new Timestamp((new Date()).getTime());
        String filePath = "output/reports/";

        StringBuilder fileName = new StringBuilder();
        fileName.append("report")
                .append("__").append(simpleDateFormat.format(new Date()))
                .append(".html");

        reportFile = new File("./" + filePath + fileName.toString());
        try{
            if(!new File(filePath).isDirectory()){
                new File(filePath).mkdir();
            }
            reportFile.createNewFile();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        catch(SecurityException e){
            e.printStackTrace();
        }

        BuzzTestAutomation.getFileManager().addFile(reportFile);
    }

    /**
     * Used to validate that the test case requirements are being fulfilled
     */
    public void validateRequirements() throws MissingConfigVariable {
        List<String> missingVars = new ArrayList<String>();
        for(TestCase testCase : testCaseList){
            for(String variable : testCase.getRequiredConfig()){
                if(!config.containsKey(variable))
                    missingVars.add(variable);
            }
        }

        if(!missingVars.isEmpty())
            throw new MissingConfigVariable(missingVars);
    }

    /**
     * Begin the test suite
     * @param driver WebDriver we are currently testing against
     */
    public void start (WebDriver driver) throws MissingConfigVariable {
        validateRequirements();

        TestRunner runner = new TestRunner(this, driver);
        activeRunners.add(runner);
        resultList.put(driver, new ArrayList<TestCase.Results>());
        runner.start();
    }


    /**
     * Grab the static logger instance
     * @return the Log4J logger instance
     */
    public static Logger log(){
        return BuzzTestAutomation.logger;
    }

    /**
     * When the test is complete
     * @param driver
     * @param results
     * @param runner
     */
    private void post (WebDriver driver, List<TestCase.Results> results, TestRunner runner) {
        resultList.get(driver).addAll(results);
        activeRunners.remove(runner);

        for(TestCase.Results resultsList : results) {
            for(TestCase.Result result : resultsList.getResultList()){
                File screenshot = result.getScreenshot();
                if(screenshot != null)
                    BuzzTestAutomation.getFileManager().addFile(screenshot);
            }
        }
    }

    /**
     * Determine if any of our threads are currently running
     * @return
     */
    public boolean isRunning () {
        Iterator<TestRunner> iterator = activeRunners.listIterator();
        while(iterator.hasNext())
            if(iterator.next().isAlive())
                return true;
        return false;
    }

    /**
     * Converts this test suite into a readable string including test results
     * @return
     */
    public String toString () {
        StringBuilder output = new StringBuilder();
        for(WebDriver key : resultList.keySet()){
            output.append("WebDriver: ").append(key.getClass().toString()).append(" \n");
            for(TestCase.Results r : resultList.get(key)){
                output.append(r.toString()).append("\n\n");
            }
        }
        return output.toString();
    }

    /**
     * Used to setup the test cases
     */
    protected abstract void setup();


    protected class TestRunner extends Thread {
        final TestSuite suite;
        final WebDriver driver;

        public TestRunner (TestSuite s, WebDriver d) {
            suite = s;
            driver = d;
        }

        public void run () {
            Iterator<TestCase> iterator = suite.getTestCaseList().listIterator();
            List<TestCase.Results> resultList = new ArrayList<Results>();
            while(iterator.hasNext()){
                TestCase testCase = iterator.next();
                TestCase.Results result = null;
                try {
                    resultList.add(testCase.run(driver));
                }
                catch(InterruptedException e) {

                }
            }
            suite.post(driver, resultList, this);
        }
    }
}
