package com.buzzpoints.selenium.main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import com.buzzpoints.selenium.api.TestConfig;
import com.buzzpoints.selenium.api.TestSuite;
import com.buzzpoints.selenium.services.Settings;
import com.buzzpoints.selenium.services.filemanager.AbstractFileManager;
import com.buzzpoints.selenium.services.filemanager.AmazonFileManager;
import com.buzzpoints.selenium.test.app.merchantportal.suites.SuiteCreateCampaigns;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by mlittman on 11/5/15.
 */
@Deprecated
public class BuzzTestAutomation {

    public static final Logger logger = LogManager.getLogger();

    private static final List<WebDriver> webDrivers = new LinkedList<WebDriver>();

    private static AbstractFileManager fileManager = new AmazonFileManager();;

    public BuzzTestAutomation () {
//        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");


//        For Executing local drivers if necessary
//        webDrivers.add(new FirefoxDriver());
//        webDrivers.add(new ChromeDriver());

        //Will try to access as many remote web drivers as available, this can be expanded as necessary
        try {
            URL hubURL = new URL((String)Settings.resolve("SeleniumHub"));
            webDrivers.add(new RemoteWebDriver(hubURL, DesiredCapabilities.firefox()));
//            webDrivers.add(new RemoteWebDriver(hubURL, DesiredCapabilities.chrome()));
//            webDrivers.add(new RemoteWebDriver(hubURL, DesiredCapabilities.internetExplorer()));
//            webDrivers.add(new RemoteWebDriver(hubURL, DesiredCapabilities.safari()));
        }
        catch(UnreachableBrowserException e){
            logger.error("RemoteWebDriver.BadServer");
        }
        catch(MalformedURLException e){
            logger.error("RemoteWebDriver.BadURL");
        }
        catch(Exception e){
            logger.error("RemoteWebDriver.UnknownError");
        }


        try {
            begin();
        }
        catch(Exception e){
            System.out.println(e);
            e.printStackTrace();
        }

        close();
    }

    public static AbstractFileManager getFileManager() {
        return fileManager;
    }

    public void run(List<TestSuite> suites){
        for (TestSuite suite : suites) {
            for(WebDriver driver : webDrivers) {
                try {
                    suite.start(driver);
                } catch (Exception e) {

                }
            }
        }

    }

    public void begin () {
        TestConfig mpConfig = new TestConfig();
        mpConfig.put("host", "http://teststaging.buzzpoints.com/business/dashboard/");
        mpConfig.put("username", "jmata+merch4@buzzpointsinc.com");
        mpConfig.put("password", "aaaaaa1");        
        mpConfig.put("merchant", "QA - Day Test");
        mpConfig.put("reward-amount", 5);
        mpConfig.put("reward-amount-other", 10);

        TestSuite mpCreateCampaigns = new SuiteCreateCampaigns(mpConfig);


        List<TestSuite> testSuites = new ArrayList<TestSuite>();
        testSuites.add(mpCreateCampaigns);

        run(testSuites);


        try{
            while(mpCreateCampaigns.isRunning()) {
                Thread.sleep(1500);
            }
        }
        catch(InterruptedException e){
            System.out.println("Something stopped me!");
        }

        System.out.println(mpCreateCampaigns);

    }

    public void close () {
        for(WebDriver driver : webDrivers)
            driver.quit();

        TestSuite.getReporter().flush();
        fileManager.uploadFiles();
    }

    public static void main (String[] args) {
        new BuzzTestAutomation();
    }
}
