# README #


### What is this repository for? ###
* Test Automation for Buzz Points services


### Installation Instructions ###
* request a bitbucket.org/buzzpoints account to have access to https://[usename]@bitbucket.org/buzzpoints/buzz-test-automation.git repository
* install git on your machine (for osx: install homebrew first from http://brew.sh/, then run command "brew install git")
* download and install Spring Tool Suite (aka:STS) editor from https://spring.io/tools/sts/all
* open STS and create a new workspace (e.g. bp-test-automation)
* download and install Lombok from https://projectlombok.org/download.html (don't forget to restart STS editor)
* click on the "Open Perspective" button (in the upper right corner) and select "Git"
* click on Clone a GIT Repository and enter git repository (you can copy from this page) and login credentials
* select [master] branch and click Next
* once the git master branch is populated on your Git Repositories left pane, right-click on top git project and select "Import Projects...", "Import as general project", click on Next, and then Finish
* switch to Spring or Java perspective view (in the upper right corner), right-click on project, select "Configure", "Convert to maven project"
* install docker from https://docs.docker.com/mac/step_one/ and follow instructions to start the server
* open command line and cd to your workspace directory, enter "mvn clean", then "mvn package", and expect "SUCCESS" build message


### Run/Debug Project ###
* docker run -d -p 4444:4444 selenium/standalone-firefox-debug:2.48.2 (reference found at https://github.com/SeleniumHQ/docker-selenium)
* mvn package
* java -jar target/buzz-test-automation-1.0.jar
* to debug from STS, right click BuzzTestAutomation.java, select "Debug As", then "Java Application"
* public reports are available on s3 upon tests completion (e.g. https://s3.amazonaws.com/test-artifacts.buzzpoints/reports/report__2016_01_07_12_24_37.html)